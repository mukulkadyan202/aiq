import unittest
import requests
from unittest.mock import patch

class TestPowerPlantsAPI(unittest.TestCase):

    @patch('requests.get')
    def test_get_powerplants_list(self, mock_get):
        mock_response = {
            "count": 11393,
            "next": "http://aiq-us-powerplants-api.onrender.com/api/powerplants/?limit=1&n=1&offset=1",
            "previous": None,
            "results": [
                {
                    "id": 382,
                    "plant_name": "Palo Verde",
                    "plant_state": "AZ",
                    "annual_net_generation": 31629862,
                    "longitude": -112.8617,
                    "latitude": 33.3881,
                    "percentage": 29.15
                }
            ]
        }
        mock_get.return_value.json.return_value = mock_response
        mock_get.return_value.status_code = 200

        response = requests.get('https://aiq-us-powerplants-api.onrender.com/api/powerplants/?n=1')

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), mock_response)

    @patch('requests.get')
    def test_get_powerplant_by_id(self, mock_get):
        mock_response = {
            "id": 1,
            "plant_name": "Agrium Kenai Nitrogen Operations",
            "plant_state": "AK",
            "annual_net_generation": 0,
            "longitude": -151.3784,
            "latitude": 60.6732,
            "percentage": 0.0
        }
        mock_get.return_value.json.return_value = mock_response
        mock_get.return_value.status_code = 200

        # Make the request and check the response
        response = requests.get('https://aiq-us-powerplants-api.onrender.com/api/powerplants/1')
        powerplant_id_status_code = response.status_code
        
        self.assertEqual(powerplant_id_status_code, 200)
        self.assertEqual(response.json(), mock_response)

    @patch('requests.get')
    def test_get_powerplants_by_state(self, mock_get):
        mock_response = {
            "count": 143,
            "next": "http://aiq-us-powerplants-api.onrender.com/api/powerplants/?limit=1&n=1&offset=1&state=AZ",
            "previous": None,
            "results": [
                {
                    "id": 382,
                    "plant_name": "Palo Verde",
                    "plant_state": "AZ",
                    "annual_net_generation": 31629862,
                    "longitude": -112.8617,
                    "latitude": 33.3881,
                    "percentage": 29.15
                }
            ]
        }
        mock_get.return_value.json.return_value = mock_response
        mock_get.return_value.status_code = 200

        response = requests.get('https://aiq-us-powerplants-api.onrender.com/api/powerplants/?state=AZ&n=1')

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), mock_response)

    @patch('requests.get')
    def test_get_powerplants_by_min_net_generation(self, mock_get):
        mock_response = {
            "count": 361,
            "next": "http://aiq-us-powerplants-api.onrender.com/api/powerplants/?limit=1&min_net_generation=3140000&n=1&offset=1",
            "previous": None,
            "results": [
                {
                    "id": 382,
                    "plant_name": "Palo Verde",
                    "plant_state": "AZ",
                    "annual_net_generation": 31629862,
                    "longitude": -112.8617,
                    "latitude": 33.3881,
                    "percentage": 29.15
                }
            ]
        }
        mock_get.return_value.json.return_value = mock_response
        mock_get.return_value.status_code = 200

        response = requests.get('https://aiq-us-powerplants-api.onrender.com/api/powerplants/?min_net_generation=3140000&n=1')

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), mock_response)

    @patch('requests.get')
    def test_get_powerplants_by_max_net_generation(self, mock_get):
        mock_response = {
            "count": 11032,
            "next": "http://aiq-us-powerplants-api.onrender.com/api/powerplants/?limit=1&max_net_generation=3140000&n=1&offset=1",
            "previous": None,
            "results": [
                {
                    "id": 343,
                    "plant_name": "Gila River Power Block 2",
                    "plant_state": "AZ",
                    "annual_net_generation": 3130183,
                    "longitude": -112.694444,
                    "latitude": 32.975,
                    "percentage": 2.88
                }
            ]
        }
        mock_get.return_value.json.return_value = mock_response
        mock_get.return_value.status_code = 200

        response = requests.get('https://aiq-us-powerplants-api.onrender.com/api/powerplants/?max_net_generation=3140000&n=1')

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), mock_response)
        
    @patch('requests.get')
    def test_api_throttling(self, mock_get):
        # Simulate throttling response
        mock_response = {
            "detail": "Request was throttled. Expected available in 1 minute."
        }
        mock_get.return_value.json.return_value = mock_response
        mock_get.return_value.status_code = 429

        # Send requests more than 100 times
        response={}
        for _ in range(101):
            response = requests.get('https://aiq-us-powerplants-api.onrender.com/api/powerplants/')
            self.assertEqual(response.status_code, 429)
            self.assertEqual(response.json(), mock_response)


if __name__ == '__main__':
    unittest.main()
